Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, skip: :all

  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :sessions, :only => [:create, :destroy]
      resources :registrations, :only => [:create]
      resources :matches, :only => [:index, :create, :show] do
        resource :status, :only => [:update], :controller => "matches/statuses"
        resource :result, :only => [:update], :controller => "matches/results"
      end
      resources :teams, :only => [:index]
      resources :platforms, :only => [:index]
      resources :championships, :only => [:index, :create, :show]
      resources :players, :only => [:index]
      resource :player, :only => [:update, :show]

      namespace :user do
        resources :devices, :only => [:create, :destroy]
      end
    end
  end

  root to: 'root#index'
end
