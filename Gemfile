source 'https://rubygems.org'

ruby "2.2.2"

gem 'rails', '~> 4.2'
gem "pg"
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0',          group: :doc
gem 'bcrypt', '~> 3.1.7'

gem "bootstrap-sass"
gem "font-awesome-sass"
gem 'select2-rails'
gem 'pundit'
gem 'kaminari'
gem 'devise'
gem 'pg_search'
gem 'paperclip'
gem 'aws-sdk'
gem 'rack-cors'
gem 'burgundy'
gem 'activeadmin', github: 'activeadmin'
gem 'houston'
gem 'carrierwave'
gem 'fog'

group :production do
  gem "newrelic_rpm"
end

group :production, :staging do
  gem 'rails_12factor'
  gem 'puma'
end

group :development do
  gem "dotenv-rails"
  gem "spring"
  gem "spring-commands-rspec"
end

group :development, :test do
  gem "awesome_print"
  gem "factory_girl_rails"
  gem "pry-rails"
  gem 'faker'
  gem "rspec"
  gem "rspec-rails"
end

group :test do
  gem "shoulda-matchers", '= 2.5.0', require: false
  gem "capybara-webkit", ">= 1.2.0"
  gem "database_cleaner"
  gem "formulaic"
  gem "timecop"
  gem "poltergeist"
end
