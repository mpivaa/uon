class CreateChampionship < ActiveRecord::Migration
  def change
    create_table(:championships) do |t|
      t.string :name
      t.date :start_at
      t.date :end_at
    end
  end
end
