class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :token
      t.references :user, index: true
      t.integer :os

      t.timestamps null: false
    end
  end
end
