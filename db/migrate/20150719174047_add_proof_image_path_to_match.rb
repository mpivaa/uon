class AddProofImagePathToMatch < ActiveRecord::Migration
  def change
    add_column :matches, :proof_image_path, :string
  end
end
