class CreateMatchPlayer < ActiveRecord::Migration
  def change
    create_table :match_players do |t|
      t.references :match, index: true
      t.references :player, index: true
      t.integer :status
      t.integer :score
      t.integer :player_type
    end
  end
end
