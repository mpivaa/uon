class PlatformAsAssociationInPlayer < ActiveRecord::Migration
  def change
    remove_column :players, :platform
    add_reference :players, :platform, index: true 
  end
end
