class CreateMatch < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :status
      t.datetime :scheduled_date
      t.datetime :started_at
      t.datetime :finished_at
      t.references :created_by, index: true
      t.references :player_won, index: true
      t.integer :match_type
    end
  end
end
