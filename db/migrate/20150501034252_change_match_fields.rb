class ChangeMatchFields < ActiveRecord::Migration
  def change
    change_table :matches do |t|
      t.timestamps
      t.remove :started_at
      t.remove :scheduled_date
      t.datetime :scheduled_for
      t.integer :result_type
    end
  end
end
