# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Platform.create([
  { name: 'Xbox One' },
  { name: 'PS4' },
  { name: 'Xbox 360' },
  { name: 'PS3' }
])

"Atlético Mineiro
Atlético Paranaense
Bahia
Botafogo
Chapecoense
Corinthians
Coritiba
Criciúma
Cruzeiro
Figueirense
Flamengo
Fluminense
Goiás
Grêmio
Internacional
Palmeiras
Santos
Sport
São Paulo".split("\n").each do |team_name|
  Team.create(name: team_name)
end
