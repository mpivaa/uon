ActiveAdmin.register Match do
  index do
    column :scheduled_for
    column :match_type
    column :status

    column 'Home player' do |match|
      match.home.player.nickname
    end

    column 'Home score' do |match|
      match.home.score
    end

    column 'Away player' do |match|
      match.away.player.nickname
    end

    column 'Away score' do |match|
      match.away.score
    end

    column 'Proof image' do |match|
      if match.proof_image_path.url
        link_to "Image",  match.proof_image_path.url, target: "_blank"
      end
    end
  end
end
