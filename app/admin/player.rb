ActiveAdmin.register Player do
  index do
    column :email do |player|
      player.user.email
    end
    column :nickname
    column :platform do |player|
      player.platform.name
    end
    column :created_at
  end
end
