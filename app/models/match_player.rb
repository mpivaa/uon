class MatchPlayer < ActiveRecord::Base
  enum status: [:waiting, :accepted, :rejected]
  enum player_type: [:away, :home]
  belongs_to :player
  belongs_to :match
  validates_presence_of :player
  validates_presence_of :status
  validates_presence_of :match

  after_initialize do
    self.status ||= :waiting
    self.player_type ||= :home
  end
end
