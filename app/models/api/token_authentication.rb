module Api::TokenAuthentication
  def authenticate_user_from_token!
    authenticate_or_request_with_http_token do |token, options|
      user = User.find_by authentication_token: token
      sign_in user, store: false
    end
  end
end
