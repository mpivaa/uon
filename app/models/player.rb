class Player < ActiveRecord::Base
  belongs_to :user, dependent: :destroy
  belongs_to :team
  belongs_to :platform
  has_many :match_players
  has_many :matches, through: :match_players
  validates_presence_of :platform, :nickname
  validates_uniqueness_of :nickname, scope: :platform
end
