class Device < ActiveRecord::Base
  validates_uniqueness_of :token, scope: :user_id
  belongs_to :user
  validates_presence_of :token, :user, :os

  enum os: [:ios, :android, :wphone]
end
