class PushNotification
  def initialize(devices, message)
    @message = message
    @devices = devices
  end

  def deliver
    @devices.each do |device|
      strategy = PushStrategy.new_strategy(device, @message)
      strategy.deliver
    end
  end
end
