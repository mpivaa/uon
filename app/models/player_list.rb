class PlayerList
  def initialize(current_player)
    @current_player = current_player
  end

  def query
    Player.where.not(id: @current_player.id)
  end
end
