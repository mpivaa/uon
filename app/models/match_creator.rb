class MatchCreator
  def initialize(options)
    options.symbolize_keys!
    @match = options.fetch(:match)
    @home_player = options.fetch(:home_player)
    @away_player = options.fetch(:away_player)
  end

  def save
    if @match.save
      if @match.match_players.create([{
        player: @home_player,
        player_type: :home
      }, {
        player: @away_player,
        player_type: :away
      }])
        send_notification
      end
    end
  end

  def send_notification
    message = "#{@home_player.nickname} invited you for a match"
    notification = PushNotification.new(@away_player.user.devices, message)
    notification.deliver
  end
end
