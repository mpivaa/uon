class PushStrategy
  @@strategies = HashWithIndifferentAccess.new

  def self.register_strategy(os, klass)
    @@strategies[os] = klass
  end

  def self.new_strategy(device, message)
    @@strategies[device.os].new(device, message)
  end

  def initialize(device, message)
    @device = device
    @message = message
  end

  def deliver
    Rails.logger.info "Sending push notification to device: #{@device.os}|#{@device.token}"
  end

  require 'push_strategies/ios'
end
