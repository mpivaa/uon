class Match < ActiveRecord::Base
  enum status: [:waiting, :accepted, :finished, :contested, :rejected]
  enum match_type: [:friendly, :championship]
  enum result_type: [:draw, :w_o, :player_victory]

  has_many :match_players, dependent: :destroy
  has_many :players, through: :match_players

  validates_presence_of :match_type
  validates_presence_of :status

  mount_uploader :proof_image_path, ProofImageUploader

  after_initialize do
    self.status ||= :waiting
  end

  [:home, :away].each do |player_type|
    define_method player_type do
      match_players.
        where(player_type: MatchPlayer.player_types[player_type]).
        take
    end

    define_method "#{player_type}=" do |match_player|
      match_player.match = self
      match_player.player_type = player_type
      match_player.save
    end
  end
end
