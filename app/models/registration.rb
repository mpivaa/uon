class Registration
  def initialize(params)
    @params = params
    @user = User.new(user_params)
    @player = Player.new(player_params)
  end

  def user_params
    @params.require(:user).permit(:email, :password, :password_confirmation)
  end

  def player_params
    @params.require(:player).permit(:nickname, :team_id, :platform_id)
  end

  def valid?
    ([@user, @player].map(&:valid?)).all?
  end

  def save
    if valid? && @user.save
      @player.user_id = @user.id
      @player.save
    end
  end

  def errors
    {
      player: @player.errors,
      user: @user.errors
    }
  end

  def as_json(*args)
    @user.as_json(*args)
  end
end
