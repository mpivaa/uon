module PushStrategies
  class IOS < PushStrategy
    register_strategy :ios, self

    def initialize(device, message)
      super(device, message)
      @apn = Rails.configuration.houston_client
      @notification = Houston::Notification.new(device: @device.token)
      @notification.alert = message
    end

    def deliver
      @apn.push(@notification)
      super
    end
  end
end
