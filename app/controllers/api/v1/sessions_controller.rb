class Api::V1::SessionsController < Api::V1::BaseController
  before_filter :allow_params_authentication!, :only => :create

  def create
    user = authenticate_user!
    sign_in user
    render json: user
  end

  def destroy
    sign_out
    redirect_to root_path
  end
end
