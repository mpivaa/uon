class Api::V1::ChampionshipsController < Api::V1::BaseController
  respond_to :json
  before_filter :authenticate_user_from_token!

  def index
    respond_with('api_v1', Championship.all)
  end

  def create
    @championship = Championship.new(championship_params)
    @championship.save
    respond_with('api_v1', @championship)
  end

  def show
    @championship = Championship.find params[:id]
    respond_with('api_v1', @championship)
  end

  protected
  def championship_params
    params.require(:championship).permit(:name, :start_at, :end_at)
  end
end
