class Api::V1::TeamsController < Api::V1::BaseController 
  respond_to :json

  def index
    respond_with('api_v1', Team.all)
  end
end
