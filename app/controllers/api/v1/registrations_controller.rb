class Api::V1::RegistrationsController < Api::V1::BaseController
  def create
    registration = Registration.new(params)
    if registration.save
      render :json=> registration, :status=>201
    else
      render :json=> registration.errors, :status=>422
    end
  end
end
