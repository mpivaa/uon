class Api::V1::PlatformsController < Api::V1::BaseController
  respond_to :json
  
  def index
    respond_with(Platform.all)
  end
end
