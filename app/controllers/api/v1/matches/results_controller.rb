class Api::V1::Matches::ResultsController < Api::V1::BaseController
  respond_to :json
  before_filter :authenticate_user_from_token!
  before_filter :set_match

  def update
    @match.home.update(score: params[:home_score])
    @match.away.update(score: params[:away_score])
    @match.status = :finished
    proof_image_file do |f|
      @match.proof_image_path = f
    end

    if @match.save
      message = "#{@match.home.player.nickname} #{@match.home.score} x #{@match.away.score} #{@match.away.player.nickname}"
      notification = PushNotification.new(@match.away.player.user.devices, message)
      notification.deliver
      head 200
    else
      head 400
    end
  end

  private
  def set_match
    @match = Match.find(params[:match_id])
  end

  def proof_image_file
    proof_image_raw = params[:proof_image_path].match(/data:(.*);base64,(.*)/)
    ext = proof_image_raw[1].match(/image\/(.*)/)[1]
    Tempfile.open(["proof-image", ".#{ext}"]) do |f|
      f.binmode
      f.write(Base64.decode64(proof_image_raw[2]))
      f.rewind
      yield(f)
    end
  end
end
