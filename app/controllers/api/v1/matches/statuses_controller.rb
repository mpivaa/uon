class Api::V1::Matches::StatusesController < Api::V1::BaseController
  respond_to :json
  before_filter :authenticate_user_from_token!

  def update
    if match.update(status: params[:status])
      message = "#{match.away.player.nickname} #{match.status} your match invitation"
      notification = PushNotification.new(match.home.player.user.devices,
                                          message)
      notification.deliver
      head 200
    else
      head 400
    end
  end

  private
  def match
    Match.find(params[:match_id])
  end
end
