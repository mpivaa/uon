class Api::V1::MatchesController < Api::V1::BaseController
  respond_to :json
  before_filter :authenticate_user_from_token!

  def index
    @matches = current_user.
      player.
      matches.
      order(scheduled_for: :desc)

    respond_with('api_v1', @matches)
  end

  def create
    @match = Match.new match_params
    MatchCreator.new({
      match: @match,
      home_player: current_user.player,
      away_player: away_player
    }).save
    respond_with('api_v1', @match)
  end

  def show
    respond_with('api_v1', match)
  end

  protected
    def match_params
      params.require(:match).permit(:scheduled_for, :match_type)
    end

    def away_player
      Player.find params[:away_player_id]
    end

    def match
      @match = Match.find params[:id]
    end
end
