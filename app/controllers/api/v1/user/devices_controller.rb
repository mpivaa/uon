class Api::V1::User::DevicesController < Api::V1::BaseController
  respond_to :json

  before_filter :authenticate_user_from_token!

  def create
    device = Device.find_or_create_by(device_params)
    respond_with("api_v1_user", device)
  end

  def destroy
    current_user.devices.find(params[:id]).destroy
    head 201
  end

  private
  def device_params
    params.require(:device)
      .permit(:token, :os)
      .merge(user: current_user)
  end
end
