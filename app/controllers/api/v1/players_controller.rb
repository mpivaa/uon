class Api::V1::PlayersController < Api::V1::BaseController
  respond_to :json
  before_filter :authenticate_user_from_token!

  def index
    respond_with('api_v1', Player.where.not(id: current_user.player.id))
  end

  def show
    respond_with('api_v1', current_user.player)
  end

  def update
    player = current_user.player
    unless player.update(player_params)
      render :json=>player.errors, :status=>:unprocessable_entity
    else
      render :json=>player
    end
  end

  protected
    def player_params
      params.require(:player).permit(:team_id, :platform_id, :nickname)
    end
end
