class Api::BaseController < ApplicationController
  include Api::TokenAuthentication
  skip_before_action :verify_authenticity_token
  respond_to :json
end
