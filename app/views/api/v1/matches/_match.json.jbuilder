json.extract! match, :id, :scheduled_for, :status, :finished_at, :match_type, :result_type
json.url api_v1_match_path(match)
json.away do
  json.extract! match.away, :status, :score
  json.extract! match.away.player, :id, :nickname
end

json.home do
  json.extract! match.home, :status, :score
  json.extract! match.home.player, :id, :nickname
end
