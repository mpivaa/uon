FactoryGirl.define do
  factory :platform do
    name "Platform"

    trait :xbox do
      name "Xbox"
    end

    trait :ps4 do
      name "PS4"
    end
  end
end
