FactoryGirl.define do
  factory :championship do
    sequence(:name) { |n| "Liga A#{n}" }
    start_at Date.today
    end_at Date.tomorrow
  end
end
