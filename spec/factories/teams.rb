FactoryGirl.define do
  factory :team do
    name "Palmeiras"
    flag_url "http://www.palmeiras.com.br/upload/images/palmeiras.png"
  end
end
