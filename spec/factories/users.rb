FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "teste#{n}@teste.com.br" }
    password '12345678'
    confirmed_at Time.now

    transient do
      devices_count 3
    end

    trait :unconfirmed do
      confirmed_at nil
    end

    trait :with_player do
      player
    end

    trait :with_devices do
      after(:create) do |user, evaluator|
        create_list(:device, evaluator.devices_count, user: user)
      end
    end
  end
end
