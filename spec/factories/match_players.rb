FactoryGirl.define do
  factory :match_player do
    player
    match

    trait :away do
      player_type 'away'
    end

    trait :home do
      player_type 'home'
    end
  end
end
