FactoryGirl.define do
  factory :player do
    sequence(:nickname) { |n| "nickname#{n}" }
    user
    team
    platform
  end
end
