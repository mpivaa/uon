FactoryGirl.define do
  factory :match do
    status 'waiting'
    match_type 'friendly'
    scheduled_for DateTime.now

    trait :with_players do
      after(:create) do |match|
        create(:match_player, :away, match: match)
        create(:match_player, :home, match: match)
      end
    end

    trait :accepted do
      with_players
      status 'accepted'
    end

    trait :finished do
      with_players
      finished_at DateTime.now
      status 'finished'
    end
  end
end
