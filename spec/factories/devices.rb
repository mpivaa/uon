FactoryGirl.define do
  factory :device do
    sequence(:token)
    association :user
    os 'ios'

    trait :ios do
      os 'ios'
    end

    trait :android do
      os 'android'
    end
  end
end
