module ApiHelper
  def json
    @json ||= JSON.parse(response.body)
  end

  def configure_user(user)
    @user = user
  end

  def inject_token(params={}, user=@user)
    params.merge({
      authorization: ActionController::HttpAuthentication::Token.encode_credentials(user.authentication_token)
    })
  end

  shared_examples "require authentication" do |block|
    let(:user) { create(:user, :with_player) }

    context 'unauthenticated request' do
      it 'returns 401' do
        instance_exec({}, &block)
        expect(response.status).to eq(401)
      end
    end

    context 'authenticated request' do
      it 'returns 201' do
        instance_exec(inject_token({}, user), &block)
        expect(response.status).to eq(201)
      end
    end
  end
end
