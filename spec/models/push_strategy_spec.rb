require 'rails_helper'

describe PushStrategy do
  let(:default_strategy) { class_double("default strategy") }
  let(:default_strategy_instance) { double(default_strategy) }
  let(:ios_device) { create(:device, :ios) }

  before do
    PushStrategy.register_strategy(:ios, default_strategy)
  end

  describe '.register_strategy' do
    it 'should register the new strategy' do
      strategy_klass = PushStrategy.class_variable_get(:@@strategies)['ios']
      expect(strategy_klass).to be(default_strategy)
    end
  end

  describe '.new_strategy' do
    it 'instantiates a new strategy for the device OS' do
      allow(default_strategy).to receive(:new).with(ios_device, "olá") { default_strategy_instance }
      strategy = PushStrategy.new_strategy(ios_device, "olá")
      expect(strategy).to be(default_strategy_instance)
    end
  end
end

