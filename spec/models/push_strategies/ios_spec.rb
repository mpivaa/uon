require 'rails_helper'

describe PushStrategies::IOS do
  let(:device) { create(:device, :ios) }
  let(:apn) { double("APN") }

  before do
    allow(Rails.configuration).to receive(:houston_client) { apn }
    allow(apn).to receive(:push)
  end

  describe '#deliver' do
    it 'should deliver to responsible sender class' do
      strategy = PushStrategies::IOS.new(device, double)
      strategy.deliver
      expect(apn).to have_received(:push)
    end
  end
end
