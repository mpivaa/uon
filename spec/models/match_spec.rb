require 'rails_helper'

describe Match do
  let(:match) { create(:match) }
  it { is_expected.to validate_presence_of(:match_type) }
  it { is_expected.to validate_presence_of(:status) }
  it { is_expected.to have_many(:players) }

  [:home, :away].each do |player_type|
    describe "##{player_type}" do
      let!(:match_player) { create(:match_player, player_type, match: match) }
      it 'returns the match player' do
        expect(match.send(player_type)).to eq(match_player)
      end
    end

    describe "##{player_type}=" do
      let!(:match_player) { create(:match_player) }
      it 'sets a match player' do
        match.send("#{player_type}=", match_player)
        expect(match.send(player_type)).to eq(match_player)
      end
    end
  end
end
