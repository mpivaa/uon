require 'rails_helper'

describe MatchCreator do
  let(:match) { build(:match) }
  let(:home_player) { create(:user, :with_player, :with_devices).player }
  let(:away_player) { create(:user, :with_player, :with_devices).player }
  let(:away_devices) { away_player.user.devices }
  let(:push_message) { "#{home_player.nickname} invited you for a match" }
  let(:push_notification) { double(PushNotification, deliver: true) }
  let(:default_options) {
    {
      match: match,
      home_player: home_player,
      away_player: away_player
    }
  }

  before do
    allow(PushNotification).to receive(:new)
      .with(away_devices, push_message) { push_notification }
  end

  describe '#save' do
    it 'saves the match' do
      expect(
        MatchCreator.new(default_options).save
      ).to be_truthy
      expect(match.persisted?).to be(true)
    end

    [:home, :away].each do |type|
      it "creates the #{type} player" do
        MatchCreator.new(default_options).save
        expect(match.players).to include(self.send("#{type}_player"))
        match_player = match.send(type)
        expect(match_player.send("#{type}?")).to be(true)
      end
    end
  end

  describe '#send_notification' do

    it 'sends a push notification' do
      MatchCreator.new(default_options).send_notification
      expect(push_notification).to have_received(:deliver)
    end
  end
end
