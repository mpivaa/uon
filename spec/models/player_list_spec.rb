require 'rails_helper'

describe PlayerList do
  let(:current_player) { create(:player) }
  let(:player_list) { PlayerList.new(current_player) }

  before do
    create(:player)
  end

  describe '#query' do
    it 'returns all players without the current one' do
      expect(player_list.query.count).to eq(1)
    end
  end
end
