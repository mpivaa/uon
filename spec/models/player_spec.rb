require 'rails_helper'

RSpec.describe Player, :type => :model do
  it { is_expected.to have_many(:matches) }
  it { is_expected.to have_many(:match_players) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:platform) }
  it { is_expected.to belong_to(:team) }
  it { is_expected.to validate_presence_of(:platform) }
  xit { is_expected.to validate_presence_of(:team) }
  xit { is_expected.to validate_presence_of(:user) }
  it { is_expected.to validate_presence_of(:nickname) }
  it { is_expected.to validate_uniqueness_of(:nickname) }
end
