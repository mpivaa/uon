require 'rails_helper'

describe PushNotification do
  let(:ios_device) { create(:device, :ios) }
  let(:android_device) { create(:device, :android) }
  let(:default_push_strategy) { double("default push strategy") }

  before do
    allow(PushStrategy).to receive(:new_strategy) { default_push_strategy }
    allow(default_push_strategy).to receive(:deliver)
  end

  describe "#deliver" do
    it "should delegate deliver to device OS strategy" do
      notification = PushNotification.new([ios_device, android_device], "olá")
      notification.deliver
      expect(default_push_strategy).to have_received(:deliver).twice
    end
  end
end
