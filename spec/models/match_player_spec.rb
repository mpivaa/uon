require 'rails_helper'

describe MatchPlayer do
  it { is_expected.to belong_to(:player) }
  it { is_expected.to belong_to(:match) }
  it { is_expected.to validate_presence_of(:status) }
  it { is_expected.to validate_presence_of(:player) }
  it { is_expected.to validate_presence_of(:match) }
end
