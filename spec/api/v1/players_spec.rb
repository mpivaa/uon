require 'rails_helper'

describe Api::V1::PlayersController do
  let(:user) { create(:user, :with_player) }
  describe 'GET /index' do
    before do
      create(:player)
    end
    context 'user is logged' do
      xit 'returns a list of players without current one' do
        get api_v1_players_path, {}, inject_token({}, user)
        expect(json.count).to eq(1)
      end
    end
  end
end
