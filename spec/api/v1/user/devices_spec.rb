require 'rails_helper'

describe Api::V1::User::DevicesController do
  let(:user) { create(:user, :with_player) }
  let(:device) { double("Device", token: "123456") }

  describe 'CREATE Device' do
    let(:params) {
      {
        device: {
          token: "123456",
          os: "ios"
        }
      }
    }

    include_examples "require authentication", -> (headers) {
      post api_v1_user_devices_path, params, headers
    }

    context 'user is logged' do
      it 'creates a new device below current user' do
        expect {
          post api_v1_user_devices_path, params, inject_token({}, user)
        }.to change { user.devices.count }.by 1
      end

      context 'device is already created' do
        before do
          user.devices.create(params[:device])
        end

        it 'fail silently' do
          expect {
            post api_v1_user_devices_path, params, inject_token({}, user)
          }.to change { user.devices.count }.by 0
        end
      end
    end
  end

  describe "DESTROY Device" do
    let!(:device) { create(:device, user: user) }

    it "destroy device" do
      expect {
        delete api_v1_user_device_path(device), {}, inject_token({}, user)
      }.to change { user.devices.count }.by(-1)
    end

    context 'device not belong to user' do
      let!(:device) { create(:device) }

      it "dont destroy device" do
        expect {
          delete api_v1_user_device_path(device), {}, inject_token({}, user)
        }.to raise_exception
      end
    end
  end
end
