require 'rails_helper'

describe 'Registrations API v1' do
  let(:user_attributes) {
    {
      email: 'teste@teste.com.br',
      password: 'teste12345678',
      password_confirmation: 'teste12345678'
    }
  }
  let!(:player_attributes) {
    build(:player).attributes
  }

  describe 'POST /registrations' do
    context 'valid request' do
      it 'registers a user' do
        expect {
          post api_v1_registrations_path, { user: user_attributes, player: player_attributes }, { "Accept" => "application/json" }
        }.to change { User.count + Player.count }.by(2)
        body = JSON.parse(response.body)
        expect(response.status).to eq(201)
        expect(body['email']).to eq(user_attributes[:email])
        expect(User.last.email).to eq(user_attributes[:email])
      end
    end

    context 'invalid request, password not sent' do
      it 'returns 422 and the error' do
        expect {
          post api_v1_registrations_path, { user: {email: 'teste@teste.com'}, player: player_attributes }, { "Accept" => "application/json" }
        }.to change { User.count }.by(0)
        expect(response.status).to eq(422)
      end
    end
  end
end
