require 'rails_helper'

describe Api::V1::Matches::StatusesController do
  let(:match) { create(:match, :with_players) }
  let(:user) { create(:user, :with_player) }

  describe '#update' do
    it 'updates the match status' do
      put api_v1_match_status_path(match), { status: 'accepted' }, inject_token({}, user)
      match.reload
      expect(match.status).to eq('accepted')
    end
  end
end
