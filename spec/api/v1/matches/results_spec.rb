require 'rails_helper'

describe Api::V1::Matches::ResultsController do
  let(:match) { create(:match, :with_players) }
  let(:user) { create(:user, :with_player) }
  let(:params) {
    {
      'away_score': 1,
      'home_score': 0,
      'proof_image_path': 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
    }
  }

  describe '#update' do
    it 'updates the match results' do
      put api_v1_match_result_path(match), params, inject_token({}, user)

      match.reload

      expect(match.home.score).to eq(0)
      expect(match.away.score).to eq(1)
      expect(match.status).to eq('finished')
      expect(match.proof_image_path.url).not_to be_nil
    end
  end
end
