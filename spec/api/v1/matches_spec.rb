require 'rails_helper'

describe Api::V1::MatchesController do
  let(:user) {
    create(:user, :with_player)
  }

  let(:player) {
    create(:player)
  }

  let(:valid_attributes) {
    attributes_for(:match)
  }

  let(:match) {
    create(:match, :with_players)
  }

  let(:match_user) {
    match.home.player.user
  }

  describe 'GET /matches' do
    it 'returns a list of matches that the user is related' do
      get api_v1_matches_path, {}, inject_token({}, match_user)
      expect(json.count).to eq(1)
    end
  end

  describe 'POST /matches' do
    before do
      configure_user user
    end

    context 'valid request' do
      it 'returns true and the match' do
        expect {
          post(api_v1_matches_path,
            { match: valid_attributes, away_player_id: player.id },
            inject_token())
        }.to change { Match.count }.by(1)
        expect(response.status).to eq(201)
      end

      it 'assigns the home player as the player that created the match' do
        post(api_v1_matches_path,
             { match: valid_attributes, away_player_id: player.id },
             inject_token())
        match = Match.last
        expect(match.home.player).to eq(user.player)
      end
    end
  end

  describe 'GET /matches/:id' do
    it 'returns the match' do
      get api_v1_match_path(match), {}, inject_token({}, match_user)
      expect(json['id']).to eq(match.id)
    end
  end
end
