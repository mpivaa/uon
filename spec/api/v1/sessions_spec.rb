require 'rails_helper'

describe 'Session API v1' do
  let(:user) { create(:user) }
  let(:params) {
    {
      user: {
        email: user.email,
        password: user.password
      }
    }
  }
  let(:accept_header) {
    {
      Accept: "application/json"
    }
  }

  describe 'POST /sessions' do
    context 'authenticate with a valid user' do
      it 'returns 201' do
        post api_v1_sessions_path, params, accept_header
        expect(response.status).to eq(200)
        expect(json['authentication_token']).to eq(user.authentication_token)
      end
    end

    context 'authenticate with an invalid user' do
      let(:wrong_params) {
        params.merge(user: { password: '123def' })
      }
      it 'returns 401' do
        post api_v1_sessions_path, wrong_params, accept_header
        expect(response.status).to eq(401)
        expect(json).to have_key('error')
      end
    end
  end
end
