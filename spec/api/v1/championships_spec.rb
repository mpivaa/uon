require 'rails_helper'

describe 'Championships API' do
  before(:each) do
    configure_user(create(:user))
  end

  describe 'GET v1/championships' do
    before(:each) do
      @championships = []
      5.times do
        @championships << create(:championship)
      end

    end

    it 'returns all the championships' do
      get api_v1_championships_path, {}, inject_token
      expect(response).to be_success
      expect(json.count).to eql(5)
      expect(json.map {|i| i['name']}).to(
        include(*@championships.map(&:name))
      )
    end

    context 'invalid user' do
      before(:each) { get api_v1_championships_path }
      it { expect(response).not_to be_success }
    end
  end

  describe 'POST v1/championships' do
    context 'valid championship' do
      it 'creates a new championship' do
        attributes = { championship: attributes_for(:championship) }
        expect {
          post api_v1_championships_path, attributes, inject_token
        }.to change { Championship.count }
        expect(Championship.last.attributes.values).to(
          include(*attributes[:championship].values)
        )
      end
    end

    context 'invalid championship' do
      before(:each) do
        post api_v1_championships_path,
          championship: {}
      end

      it { expect(response).not_to be_success }
    end

    context 'invalid user' do
      before(:each) do
        post api_v1_championships_path,
          championship: attributes_for(:championship)
      end

      it { expect(response).not_to be_success }
    end
  end

  describe 'GET /v1/championship/:id' do
    let(:championship) { create(:championship) }

    context 'valid user' do
      before(:each) {
        get api_v1_championship_path(championship), {}, inject_token
      }
      it { expect(response).to be_success }
      it { expect(json['id']).to eql(championship.id) }
    end
  end
end
